import MP3 from '../../audios/background/background.mp3'
import OGG from '../../audios/background/background.ogg'

export default class Background {
  static onPreload ({ scene, key }) {
    scene.load.audio(key, [MP3, OGG])
  }

  constructor ({ scene, key }) {
    this.scene = scene
    this.music = this.scene.sound.add(key)
    this.key = key
  }

  playMusic ({ volume = 0.5, loop = true }) {
    this.music.play({ volume, loop })
    return this
  }

  stopMusic () {
    this.music.stop()
    return this
  }

  toggleMusic (isPlay) {
    if (isPlay) {
      this.playMusic({})
    } else {
      this.stopMusic()
    }
    return this
  }
}
