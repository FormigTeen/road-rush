import Main from '../scenes/Main'
import Phaser from 'phaser'
import Splash from '../scenes/Splash'
import Over from '../scenes/Over'

export const Config = {
  loader: {
    crossOrigin: 'anonymous'
  },
  type: Phaser.AUTO,
  parent: 'phaser-game',
  scene: [Splash, Main, Over]
}

export const Desktop = {
  width: 480,
  height: 640,
  ...Config
}

export const Mobile = {
  width: window.innerWidth,
  height: window.innerHeight,
  ...Config
}

export default Desktop
