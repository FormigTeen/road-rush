const isTablet = () => navigator.userAgent.indexOf('Tablet') >= 0

export default isTablet
