import Phaser from 'phaser'

export const SCORE_UPDATE = 'SCORE_UPDATE'
export const SCORE_UPDATED = 'SCORE_UPDATED'
export const START_GAME = 'START_GAME'
export const TOGGLE_SOUND = 'TOGGLE_SOUND'

export default class Router extends Phaser.Events.EventEmitter {

}
