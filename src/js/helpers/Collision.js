export default class Collision {
  static isCollide (objOne, objTwo) {
    const distX = Math.abs(objOne.x - objTwo.x)
    const distY = Math.abs(objOne.y - objTwo.y)
    return (distX <= (objOne.width / 2) && distY <= (objOne.height / 2))
  }
}
