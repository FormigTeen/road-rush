import Game from './Game'
import { Desktop, Mobile } from './config/Game'
import isMobile from './helpers/isMobile'
import isTablet from './helpers/isTablet'

window.onload = () => {
  const GameObject = new Game(isMobile() || isTablet() ? Mobile : Desktop)
  GameObject.log()
}
