import Phaser from 'phaser'
import Cars from '../../../images/cars.png'
import Collision from '../../helpers/Collision'
import WHOOSH_MP3 from '../../../audios/effects/whoosh.mp3'
import WHOOSH_OGG from '../../../audios/effects/whoosh.ogg'
import BOOM_MP3 from '../../../audios/effects/boom.mp3'
import BOOM_OGG from '../../../audios/effects/boom.ogg'

export default class Car extends Phaser.GameObjects.Sprite {
  static onPreload ({ scene, key }) {
    scene.load.spritesheet(key, Cars, {
      frameWidth: 60,
      frameHeight: 126
    })
    scene.load.audio(key.concat('-whoosh'), [WHOOSH_MP3, WHOOSH_OGG])
    scene.load.audio(key.concat('-boom'), [BOOM_MP3, BOOM_OGG])
  }

  constructor ({ scene, x, y, texture }) {
    super(scene, x, y, texture)
    this.whoosh = this.scene.sound.add(texture.concat('-whoosh'))
    this.boom = this.scene.sound.add(texture.concat('-boom'))
    this.scene.add.existing(this)
  }

  setController (control) {
    control.setInteractive()
    control.on('pointerdown', () => {
      this.setX(this.x * -1)
      this.whoosh.play({ volume: 1, loop: false })
    })
  }

  onCollide (object) {
    if (Collision.isCollide(this, object)) {
      this.setAlpha(0.5)
      this.boom.play({ volume: 1, loop: false })
    } else {
      this.setAlpha(1)
    }
  }
}
