import Phaser from 'phaser'

export default class ScoreBox extends Phaser.GameObjects.Container {
  constructor ({ scene }) {
    super(scene)
    this.text = this.scene.add.text(0, 0, 'SCORE: 0')
    this.text.setOrigin(0.5, 0.5)
    this.add(this.text)
    this.scene.add.existing(this)
    this.setPosition(this.scene.game.config.width / 8, 50)
    this.score = 0
  }

  reloadText () {
    this.text.setText('SCORE: ' + this.score)
    return this
  }

  addScoreOnEvent (eventObject, eventFlag, responseFlag = null) {
    eventObject.on(eventFlag, (value) => this.addScore(value))
    if (responseFlag) {
      eventObject.emit(responseFlag, this.score)
    }
    return this
  }

  addScore (score) {
    this.score += score
    this.reloadText()
    return this
  }
}
