import Phaser from 'phaser'
import LogoImage from '../../../images/logo.png'

export default class Logo extends Phaser.GameObjects.Image {
  static onPreload ({ scene, key }) {
    scene.load.image(key, LogoImage)
  }

  constructor ({ scene, key }) {
    super(scene, 0, 0, key)
    scene.add.existing(this)
  }
}
