import Phaser from 'phaser'
import RoadImage from './../../../images/road.jpg'
import Align from '../../helpers/Align'
import Line from '../../../images/line.png'

export default class Road extends Phaser.GameObjects.Container {
  static onPreload ({ scene, key }) {
    scene.load.image(key, RoadImage)
    scene.load.image('line', Line)
  }

  constructor ({ scene, key }) {
    super(scene)
    this.vSpace = this.scene.game.config.height / 10
    this.setImage(key)
    this.countMove = 0
    this.limitMove = 30
    this.makeLines()
  }

  setImage (key) {
    this.road = this.scene.add.image(0, 0, key)
    Align.scaleToGameWidth(this.road, 0.5)
    this.setSize(this.road.displayWidth, this.scene.game.config.height)
    this.add(this.road)
    this.setPosition(this.scene.game.config.width / 2, 0)
    this.scene.add.existing(this)
  }

  makeLines () {
    this.lineGroup = this.scene.add.group()
    for (let i = 0; i < 100; i++) {
      const line = this.scene.add.image(this.x, this.vSpace * i, 'line')
      this.lineGroup.add(line)
      line.originalY = line.y
    }
  }

  moveLines () {
    this.lineGroup.children.iterate((child) => (
      child.setY(this.countMove === this.limitMove ? child.originalY : (child.y + this.vSpace / 30))
    ))
    this.countMove = this.countMove === this.limitMove ? 0 : this.countMove + 1
  }
}
