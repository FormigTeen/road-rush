import Phaser from 'phaser'
import PoliceImage from '../../../images/pcar1.png'
import PoliceImageAlt from '../../../images/pcar2.png'
import ConeImage from '../../../images/cone.png'
import BarrierImage from '../../../images/barrier.png'

export default class Obstacle extends Phaser.GameObjects.Sprite {
  static onPreload ({ scene, key }) {
    scene.load.image(key + '-0', ConeImage)
    scene.load.image(key + '-1', PoliceImage)
    scene.load.image(key + '-2', PoliceImageAlt)
    scene.load.image(key + '-3', BarrierImage)
  }

  constructor ({ scene, x, y, velocity, texture, altKey, onClone, onDestroy }) {
    super(scene, x, y, texture + '-' + altKey)
    this.originalX = x
    this.originalY = y
    this.originalTexture = texture
    this.scene.add.existing(this)
    this.onClone = onClone
    this.onDestroy = onDestroy
    this.onClone(this)
    this.vSpace = this.scene.game.config.height / 10
    this.customVelocity = velocity || 1
    this.hasClone = false
  }

  clone () {
    const lineRandom = (Math.round(Math.random() * 100) % 2 === 0 ? 1 : -1)
    const altKeyRandom = (Math.round(Math.random() * 40) % 4)
    const velocityRandom = this.customVelocity + 0.05
    this.objectClone = new Obstacle({ velocity: velocityRandom, scene: this.scene, x: this.originalX * lineRandom, y: this.originalY, altKey: altKeyRandom, onClone: this.onClone, texture: this.originalTexture, onDestroy: this.onDestroy })
    this.onDestroy()
    this.destroy()
  }

  move () {
    if (this.hasClone) {
      return this.objectClone.move()
    } else {
      this.setY(this.y + this.vSpace / 20 * this.customVelocity)
      if (this.y > this.scene.game.config.height) {
        this.clone()
        this.hasClone = true
      }
      return this
    }
  }
}
