import Phaser from 'phaser'
import ButtonImg2 from '../../images/ui/buttons/2/1.png'
import ButtonImg1 from '../../images/ui/buttons/2/4.png'
import isMobile from '../helpers/isMobile'
import isTablet from '../helpers/isTablet'

export default class FlatButton extends Phaser.GameObjects.Container {
  constructor ({ scene, key, text }) {
    super(scene)
    this.createButton({ key, text })
    this.setHoverEvent()
  }

  createButton ({ key, text }) {
    this.button = this.scene.add.image(0, 0, key)
    this.text = this.scene.add.text(0, 0, text).setOrigin(0.5, 0.5)
    this.add(this.button)
    this.add(this.text)
    this.scene.add.existing(this)
  }

  setHoverEvent () {
    this.button.setInteractive()
    if (!isMobile() && !isTablet()) {
      this.button.on('pointerover', () => this.onOver())
      this.button.on('pointerout', () => this.onOut())
    }
  }

  onOver () {
    this.setY(this.y - 5)
  }

  onOut () {
    this.setY(this.y + 5)
  }

  setEvent (eventObject, eventFlag) {
    this.setAction(() => eventObject.emit(eventFlag))
  }

  setAction (callable) {
    this.button.on('pointerdown', callable)
  }

  static onPreload ({ scene, prefixKey }) {
    scene.load.image(prefixKey + '-1', ButtonImg1)
    scene.load.image(prefixKey + '-2', ButtonImg2)
  }
}
