import Phaser from 'phaser'
import OnIcon from './../../images/ui/icons/music_on.png'
import OffIcon from './../../images/ui/icons/music_off.png'
import BackToggle from './../../images/ui/toggles/1.png'

import Align from '../helpers/Align'

export default class ToogleButton extends Phaser.GameObjects.Container {
  static onPreload ({ scene, key }) {
    scene.load.image(key + '-icon-on', OnIcon)
    scene.load.image(key + '-icon-off', OffIcon)
    scene.load.image(key, BackToggle)
  }

  constructor ({ scene, key, isActive = false, x = 0, y = 0 }) {
    super(scene, x, y)
    this.isActive = isActive

    this.createObjects(key)
    this.setToggle()

    this.scene.add.existing(this)
  }

  setToggle () {
    this.back.setInteractive()
    this.back.on('pointerdown', () => this.toggle())
  }

  createObjects (key) {
    this.back = this.scene.add.image(0, 0, key)
    this.onIcon = this.scene.add.image(0, 0, key + '-icon-on')
    this.offIcon = this.scene.add.image(0, 0, key + '-icon-off')

    Align.scaleToGameWidth(this.back, 0.1)
    Align.scaleToGameWidth(this.onIcon, 0.05)
    Align.scaleToGameWidth(this.offIcon, 0.05)

    this.add(this.back)
    this.add(this.onIcon)
    this.add(this.offIcon)
    this.setIcons()
  }

  toggle () {
    this.isActive = !this.isActive
    this.setIcons()
    if (this.onToggle) {
      this.onToggle()
    }
  }

  setIcons () {
    this.onIcon.visible = !!this.isActive
    this.offIcon.visible = !this.isActive
  }

  setOnToggle (func) {
    this.onToggle = func
  }
}
