import Phaser from 'phaser'
import Router, { START_GAME, TOGGLE_SOUND } from '../../helpers/Router'
import Grid from '../../helpers/Grid'
import FlatButton from '../../ui/FlatButton'
import ToggleButton from '../../ui/ToggleButton'
import Logo from '../../objects/Logo'
import Align from '../../helpers/Align'
import Background from '../../audios/Background'

export default class Splash extends Phaser.Scene {
  constructor () {
    super('Splash')
  }

  preload () {
    FlatButton.onPreload({ scene: this, prefixKey: 'button' })
    Logo.onPreload({ scene: this, key: 'logo' })
    Background.onPreload({ scene: this, key: 'background-music' })
    ToggleButton.onPreload({ scene: this, key: 'toggle' })
  }

  create () {
    this.router = new Router()
    this.router.on(START_GAME, () => this.onStart())

    this.button = new FlatButton({ scene: this, key: 'button-1', text: 'Jogar' })
    this.button.setEvent(this.router, START_GAME)
    this.logo = new Logo({ scene: this, key: 'logo' })

    this.grid = new Grid({ scene: this, rows: 10, cols: 5 })
    this.grid.putAtIndex(this.button, 33)
    this.grid.putAtIndex(this.logo, 18)

    this.backgroundMusic = new Background({ scene: this, key: 'background-music' })
    this.backgroundMusic.playMusic({})
    this.router.on(TOGGLE_SOUND, (isActive) => this.backgroundMusic.toggleMusic(isActive))

    this.toggleButton = new ToggleButton({ scene: this, key: 'toggle', isActive: true })
    this.grid.putAtIndex(this.toggleButton, 43)
    this.toggleButton.setOnToggle(() => this.router.emit(TOGGLE_SOUND, this.toggleButton.isActive))

    Align.scaleToGameWidth(this.logo, 0.8)
  }

  update () {
  }

  onStart () {
    this.scene.start('Main')
  }
}
