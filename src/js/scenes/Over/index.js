import Phaser from 'phaser'
import Router from '../../helpers/Router'
import Grid from '../../helpers/Grid'
import FlatButton from '../../ui/FlatButton'
import Logo from '../../objects/Logo'
import Align from '../../helpers/Align'

export default class Over extends Phaser.Scene {
  constructor () {
    super('Over')
  }

  preload () {
    FlatButton.onPreload({ scene: this, prefixKey: 'button' })
    Logo.onPreload({ scene: this, key: 'logo' })
  }

  create () {
    this.router = new Router()
    this.router.on('play', () => this.onStart())

    this.button = new FlatButton({ scene: this, key: 'button-2', text: 'Tente Novamente' })
    this.button.setEvent(this.router, 'play')
    this.logo = new Logo({ scene: this, key: 'logo' })

    this.grid = new Grid({ scene: this, rows: 10, cols: 5 })
    this.grid.putAtIndex(this.button, 33)
    this.grid.putAtIndex(this.logo, 18)
    Align.scaleToGameWidth(this.logo, 0.8)
  }

  update () {
  }

  onStart () {
    this.scene.start('Main')
  }
}
