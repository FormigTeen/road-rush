import Phaser from 'phaser'
import RoadObject from './../../objects/Road'
import CarObject from '../../objects/Car'
import Obstacle from '../../objects/Obstacle'
import ScoreBox from '../../objects/ScoreBox'
import Router, { SCORE_UPDATED, SCORE_UPDATE } from '../../helpers/Router'
import Grid from '../../helpers/Grid'

export default class Main extends Phaser.Scene {
  constructor () {
    super('Main')
  }

  preload () {
    RoadObject.onPreload({ scene: this, key: 'road' })
    CarObject.onPreload({ scene: this, key: 'car' })
    Obstacle.onPreload({ scene: this, key: 'obstacle' })
  }

  create () {
    this.router = new Router()
    this.router.on('clicked', () => console.log('Clicado!'))

    this.textScore = (new ScoreBox({ scene: this }))
      .addScoreOnEvent(this.router, SCORE_UPDATE, SCORE_UPDATED)

    this.containerRoad = new RoadObject({ scene: this, key: 'road' })
    this.car = new CarObject({ scene: this, x: this.containerRoad.displayWidth / 5, y: this.containerRoad.displayHeight * 0.9, texture: 'car' })
    this.obstacle = new Obstacle({
      scene: this,
      x: -1 * this.containerRoad.displayWidth / 5,
      y: 0,
      texture: 'obstacle',
      altKey: (Math.round(Math.random() * 40) % 4),
      onDestroy: () => this.router.emit(SCORE_UPDATE, 1),
      onClone: (clone) => this.containerRoad.add(clone)
    })

    this.car.setController(this.containerRoad.road)
    this.containerRoad.add(this.car)

    this.grid = new Grid({ scene: this, rows: 10, cols: 5 })
  }

  update () {
    const obstacle = this.obstacle.move()
    this.containerRoad.moveLines()
    this.car.onCollide(obstacle)
  }
}
